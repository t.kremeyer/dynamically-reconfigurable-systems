library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity FIFO is
	Generic (
		constant width  : positive := 8;
		constant depth  : positive := 64
	);
	Port ( 
		clk		: in  STD_LOGIC;
		reset		: in  STD_LOGIC;
		wr_en		: in  STD_LOGIC;
		d_in		: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		rd_en		: in  STD_LOGIC;
		d_out		: out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
		empty		: out STD_LOGIC;
		full		: out STD_LOGIC
	);
end FIFO;

architecture behavioral of FIFO is

begin

	fifo_proc : process (clk)
		type FIFO_mem is array (0 to depth - 1) of STD_LOGIC_VECTOR (width - 1 downto 0);
		variable mem : FIFO_mem;
		
		variable head : natural range 0 to FIFO_DEPTH - 1;
		variable tail : natural range 0 to FIFO_DEPTH - 1;
		variable size : natural range 0 to FIFO_DEPTH - 1;
		
	begin
		if rising_edge(clk) then
			if rst = '1' then
				head := 0;
				tail := 0;
				full  <= '0';
				empty <= '1';
			else
				if (rd_en = '1') then
					if (size /= 0) then
						-- schreibe daten
						d_out <= mem(tail);
						-- tail-pointer aktualisieren
						if (tail = depth - 1) then
							tail:= 0;
						else
							tail := tail + 1;
						end if;
						
						-- infos über zustand aktualisieren leer/voll
						full <= '0';
						size := size -1;

						if (size = 0) then
							empty <= '1';
						else
							empty <= '0';
						end if; 
					end if;
				end if;
				
				if (wr_en = '1') then
					if (size /= depth) then
						-- daten schreiben
						mem(head) := d_in;
						-- head-pointer aktualisieren
						if (head = depth - 1) then
							head := 0;
						else
							head := head + 1;
						end if;
					end if;
				end if;
				
				-- infos über zustand aktualisieren leer/voll
				empty	<= '0';
				size := size + 1;				

				if (size = d) then
					full	<= '1';
				else
					full	<= '0';
				end if;
			end if;
		end if;
	end process;
		
end behavioral;
