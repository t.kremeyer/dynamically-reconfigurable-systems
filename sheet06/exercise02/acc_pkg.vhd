
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package acc_pkg is
    type instruction is record
        addr: natural range 0 to 255;
        length: natural range 0 to 255;
    end record;

    type instruction_array is array(natural range <>) of instruction;

    type acc_status is (idle, init, busy, finished);

    constant test_instructions: instruction_array(0 to 7) := (
        (addr =>   5, length =>  20),        -- 0 -> 120957‬
        (addr => 254, length =>   1),        -- 1
        (addr =>   0, length =>   1),        -- 2
        (addr => 100, length =>  17),        -- 3
        (addr =>  71, length =>   5),        -- 4 -> 14269
        (addr =>   9, length =>  73),        -- 5
        (addr =>  20, length => 191),        -- 6
        (addr =>  42, length =>  42)         -- 7
    );

end package;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.acc_pkg.all;
    
entity acc is 
port(
    instr:  in  instruction;
    din:    in  std_ulogic_vector(31 downto 0);
    clk:    in  std_ulogic;
    status: out acc_status := idle;
    addr:   out std_ulogic_vector(7 downto 0) := (others => '0');
    dout:   out std_ulogic_vector(31 downto 0) := (others => '0'));
end entity;

architecture impl of acc is
    signal sum: std_ulogic_vector(31 downto 0) := (others => '0');
    signal current_instr: instruction;
    signal current_addr: std_ulogic_vector(7 downto 0);
    signal sig_status: acc_status;
begin
main: process(clk)
begin
    if rising_edge(clk) then
        if sig_status = idle then
            sum <= (others => '0');
            status <= init;
            sig_status <= init;
        elsif sig_status = init then
            sum <= (others => '0');
            current_instr <= instr;
            addr <= std_ulogic_vector(to_unsigned(instr.addr, 8));
            current_addr <= std_ulogic_vector(to_unsigned(instr.addr, 8));
            status <= busy;
            sig_status <= busy;
        elsif sig_status = busy then
            sum <= std_ulogic_vector(unsigned(sum) + unsigned(din));
            addr <= std_ulogic_vector(unsigned(current_addr) + 1);
            current_addr <= std_ulogic_vector(unsigned(current_addr) + 1);
            dout <= std_ulogic_vector(unsigned(sum) + unsigned(din));
            if std_ulogic_vector(unsigned(current_addr) + 1) = std_ulogic_vector(to_unsigned(current_instr.addr, 8) + to_unsigned(current_instr.length, 8)) then
                status <= finished;
                sig_status <= finished;
            end if;
        elsif sig_status = finished then
            dout <= sum;
            status <= idle;
            sig_status <= idle;
        end if;
    end if;
end process;

end architecture;
