-- Testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.acc_pkg.all;

entity testbench is
end entity;

architecture tb of testbench is
    signal instr: instruction;
    signal din: std_ulogic_vector(31 downto 0);
    signal clk: std_ulogic := '0';
    signal status: acc_status;
    signal addr: std_ulogic_vector(7 downto 0);
    signal dout: std_ulogic_vector(31 downto 0);

begin

    clk <= not clk after 1 ns;

    acc: entity work.acc
    port map(
        instr => instr,
        din => din,
        clk => clk,
        status => status,
        addr => addr,
        dout => dout
    );
    
    mem: entity work.mem
    port map(
        addr => addr,
        dout => din
    );

main:process
begin
    report "Testbench Init";
    
    wait until status = idle;
    instr <= test_instructions(0);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
    
    wait until status = idle;
    instr <= test_instructions(1);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
    
    wait until status = idle;
    instr <= test_instructions(2);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
    
    wait until status = idle;
    instr <= test_instructions(3);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
    
    wait until status = idle;
    instr <= test_instructions(4);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
    
    wait until status = idle;
    instr <= test_instructions(5);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
    
    wait until status = idle;
    instr <= test_instructions(6);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
    
    wait until status = idle;
    instr <= test_instructions(7);
    wait until status = finished;
    report integer'image(to_integer(unsigned(dout)));
        
    report "DONE";
    wait;
    
    
end process;

end architecture;