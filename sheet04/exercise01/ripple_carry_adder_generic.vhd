entity ripple_carry_adder_generic is
  generic (BITWIDTH: integer);
  port    (a, b: in  bit_vector(BITWIDTH-1 downto 0);
           s:    out bit_vector(BITWIDTH downto 0));
end ripple_carry_adder_generic;

architecture structural of ripple_carry_adder_generic is

  component fulladder
    port(a, b, cin: in bit; s, cout: out bit);
  end component;

  signal c: bit_vector(BITWIDTH-1 downto 1);

begin

  fa_0: fulladder port map (a=>a(0), b=>b(0), cin=>'0', s=>s(0), cout=>c(1));

  generate_fa: for i in 1 to BITWIDTH-2 generate
    fa_i: fulladder port map (a=>a(i), b=>b(i), cin=>c(i), s=>s(i), cout=>c(i+1));
  end generate generate_fa;
  
  generate_last_fa: if BITWIDTH > 1 generate
    fa_n: fulladder port map (a=>a(BITWIDTH-1), b=>b(BITWIDTH-1), cin=>c(BITWIDTH-1), s=>s(BITWIDTH-1), cout=>s(BITWIDTH));
  end generate generate_last_fa;
end structural;