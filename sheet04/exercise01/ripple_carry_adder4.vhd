entity ripple_carry_adder_4 is
  port(a, b: in  bit_vector(3 downto 0);
       s:    out bit_vector(4 downto 0));
end ripple_carry_adder_4;

architecture structural of ripple_carry_adder_4 is

  component fulladder
    port(a, b, cin: in bit; s, cout: out bit);
  end component;

  signal c: bit_vector(3 downto 1);

begin

  fa_0: fulladder port map (a=>a(0), b=>b(0), cin=>'0', s=>s(0), cout=>c(1));

  generate_fa: for i in 1 to 2 generate
    fa_i: fulladder port map (a=>a(i), b=>b(i), cin=>c(i), s=>s(i), cout=>c(i+1));
  end generate generate_fa;
  
  fa_3: fulladder port map (a=>a(3), b=>b(3), cin=>c(3), s=>s(3), cout=>s(4));
end structural;