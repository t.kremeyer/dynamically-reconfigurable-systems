entity testbench is
end entity;

library ieee;
use ieee.numeric_bit.all;

architecture tb of testbench is
  component ripple_carry_adder_generic
    generic (BITWIDTH: integer);
    port    (a, b: in  bit_vector(BITWIDTH-1 downto 0); s: out bit_vector(BITWIDTH downto 0));
  end component;
  
  constant n: integer := 16;
  
  signal a,b: bit_vector(n-1 downto 0);
  signal s:   bit_vector(n downto 0);
  
begin

test:process
begin
  wait for 2 ns;
  report "Test 1+1";
  a <= bit_vector(to_unsigned(1, n));
  b <= bit_vector(to_unsigned(1, n));
  wait for 2 ns;
  assert to_integer(unsigned(s)) = 2
    report "1+1 != 2. Actual: " & integer'image(to_integer(unsigned(s)))
    severity failure;

  wait for 2 ns;
  report "Test 1024+32";
  a <= bit_vector(to_unsigned(1024, n));
  b <= bit_vector(to_unsigned(32, n));
  wait for 2 ns;
  assert to_integer(unsigned(s)) = 1056
    report "1+1 != 1056. Actual: " & integer'image(to_integer(unsigned(s)))
    severity failure;
    
  wait for 2 ns;
  report "Test 65535+1";
  a <= bit_vector(to_unsigned(65535, n));
  b <= bit_vector(to_unsigned(1, n));
  wait for 2 ns;
  assert to_integer(unsigned(s)) = 65536
    report "65535+1 != 65536. Actual: " & integer'image(to_integer(unsigned(s)))
    severity failure;
  
  wait for 2 ns;
  report "Test 65535+65535";
  a <= bit_vector(to_unsigned(65535, n));
  b <= bit_vector(to_unsigned(65535, n));
  wait for 2 ns;
  assert to_integer(unsigned(s)) = 131070
    report "65535+65535 != 131070. Actual: " & integer'image(to_integer(unsigned(s)))
    severity failure;
  
  wait for 2 ns;
  report "Test 123+987";
  a <= bit_vector(to_unsigned(123, n));
  b <= bit_vector(to_unsigned(987, n));
  wait for 2 ns;
  assert to_integer(unsigned(s)) = 1110
    report "123+987 != 1110. Actual: " & integer'image(to_integer(unsigned(s)))
    severity failure;
  
  wait for 2ns;
  report "Success!";
  wait;
  
end process;

dut: ripple_carry_adder_generic 
  generic map(BITWIDTH=>n)
  port map(a=>a, b=>b, s=>s);

end architecture;