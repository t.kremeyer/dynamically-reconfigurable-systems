entity fulladder is
  port(a, b, cin: in bit;
       s, cout: out bit);
end fulladder;

architecture dataflow of fulladder is
begin
  s    <= (cin and (a xnor b)) or (not cin and (a xor b));
  cout <= (cin and (a or b)) or (a and b);
end dataflow;

architecture behavioral of fulladder is
begin
calc: process(a, b, cin)
  begin
    if cin = '1' then
      s    <= a xnor b;
      cout <= a or b;
    else
      s    <= a xor b;
      cout <= a and b;
    end if;
  end process calc;
end behavioral;