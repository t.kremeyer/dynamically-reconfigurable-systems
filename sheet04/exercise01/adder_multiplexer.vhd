entity adder_multiplexer_4 is
  port (a,b,c,d: in  bit_vector(3 downto 0);
        ctrl:    in  bit;
        sum:     out bit_vector(4 downto 0));
end adder_multiplexer_4;

architecture structural of adder_multiplexer_4 is
  component ripple_carry_adder_4
    port(a, b: in  bit_vector(3 downto 0);
         s:    out bit_vector(4 downto 0));
  end component;
  
  signal x, y: bit_vector(3 downto 0);
begin

  with ctrl select x <=
    a when '0',
    c when '1';

  with ctrl select y <= 
    b when '0',
    d when '1';
  
  add: ripple_carry_adder_4 port map (a=>x, b=>y, s=>sum);

end architecture;