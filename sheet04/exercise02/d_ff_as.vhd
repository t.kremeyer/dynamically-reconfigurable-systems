entity d_ff_as is
  port( d, c, r: in bit;
        q:       out bit);
end d_ff_as;

architecture behavioral of d_ff_as is
begin
  process( c, r )
  begin
    if (r='0') then  --aktives Rücksetzen mit 0
      q <= '0';
    elsif (c'event and c='1') then  --ieee standard def für rising edge
      q <= d;
    end if;
  end process;
end behavioral;
