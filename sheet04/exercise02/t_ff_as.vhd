entity t_ff_as is
  port( t, c, r: in bit;
        q:       out bit);
end t_ff_as;

architecture behavioral of t_ff_as is
begin
  process(c, r)
  begin
    if r='1' then
      q <= '0';
    elsif rising_edge(c) then
      if t='1' then
        q <= not q;
      end if;
    end if;
  end process;
end behavioral;
