# VHDL Testbench

After each `wait until rising_edge(clk);` we had to wait some more time (e.g. 
a nanosecond) in order to make the test work. This seems to be caused by 
processing time of the components since the components only start to process 
the input when the clock edge happens.

Initially, the test case "0--" failed and the counter stayed at '0'. We 
replaced `count_reg <= count_reg - decr;` by `count_reg <= count_reg - 1;` to 
fix that, since `decr` is set to 0 when the counter is at 0.