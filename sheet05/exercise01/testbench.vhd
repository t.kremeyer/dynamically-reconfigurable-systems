-- Testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity testbench is
end entity;

architecture tb of testbench is
	signal clk: std_logic := '0';

	signal din, count: std_logic_vector(7 downto 0) := (others => '0');
	signal reset, load, up: std_logic := '0';

begin

clk <= not clk after 1 ns;


uut: entity work.up_down_counter
port map(
	clk => clk,
	reset => reset,
	load => load,
	din => din,
	up => up,
	count => count
);

main: process
begin
    report "Test Load";
    reset <= '0';
    load <= '1';
    din <= std_logic_vector(to_unsigned(42, 8));
    up <= '0';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 42
        report "Load does not work. Expected: 42. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;
    
    report "Test Reset";
    reset <= '1';
    load <= '0';
    din <= std_logic_vector(to_unsigned(42, 8));
    up <= '0';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 0
        report "Reset does not work. Expected: 0. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;
    
    report "Test 0--";
    reset <= '0';
    load <= '0';
    din <= std_logic_vector(to_unsigned(42, 8));
    up <= '0';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 255
        report "0-- does not work. Expected: 255. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;
    
    report "Test 255++";
    reset <= '0';
    load <= '0';
    din <= std_logic_vector(to_unsigned(42, 8));
    up <= '1';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 0
        report "255++ does not work. Expected: 0. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;
    
    report "Test 0++";
    reset <= '0';
    load <= '0';
    din <= std_logic_vector(to_unsigned(42, 8));
    up <= '1';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 1
        report "0++ does not work. Expected: 1. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;
    
    report "Test 1--";
    reset <= '0';
    load <= '0';
    din <= std_logic_vector(to_unsigned(42, 8));
    up <= '0';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 0
        report "1-- does not work. Expected: 0. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;
    
    report "Test Load(42) precedes Up";
    reset <= '0';
    load <= '1';
    din <= std_logic_vector(to_unsigned(42, 8));
    up <= '1';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 42
        report "Load 42 does not work. Expected: 42. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;
    
    report "Test Reset precedes Load(21)";
    reset <= '1';
    load <= '1';
    din <= std_logic_vector(to_unsigned(21, 8));
    up <= '1';
    wait until rising_edge(clk);
    wait for 1 ns;
    assert to_integer(unsigned(count)) = 0
        report "Reset does not work. Expected: 0. Actual: " & integer'image(to_integer(unsigned(count))) & "."
        severity failure;

    wait for 1 ns;
    report "Success!";
    wait;

end process;


end architecture;
