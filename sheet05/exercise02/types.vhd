type DISTANCE is range 0 to 2147483647
  units
    um;              -- micron
    mm = 1000 um;    -- millimeter
	cm = 10 mm;      -- centimeter
    d_in = 25400 um; -- inch
	d_ft = 12 d_in;  -- foot
	d_yd = 3 d_ft;   -- yard
	
  end units DISTANCE;

type AREA is range 0 to 2147483647
  units
    um2;                -- micron^2
	cm2 = 1E8 um2;      -- centimeter^2
    m2 = 10000 cm2;     -- meter^2
    d_in2 = 6452E5 um2; -- inch^2

  end units AREA;


